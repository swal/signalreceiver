import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DummyClient {
	static String filename = "output.sasx";
	static int samplerate = 20000;  // 20 kHz
	static int timeintval = 3;      // 3 seconds
	
	
	private static void flushData(String filename, List<Integer> data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
		writer.write("Signalanalyzer\nRate:" + Integer.toString(samplerate) + "\nUnit:val\nSamples:" + Integer.toString(data.size()) + "\n");
		for (Integer msv : data) {
			writer.write(Integer.toString(msv) + "\n");
		}
		writer.close();
	}
	
	
	public static void main(String[] args) throws InterruptedException, IOException {
		int num_samples = timeintval * samplerate;
		int dummy_data = 0;
		while (true) {
			List<Integer> sample_list = new ArrayList<>();
			for(int sample_count = 0; sample_count < num_samples; sample_count++) {
				sample_list.add(dummy_data);
				dummy_data++;
			}
			flushData(filename, sample_list);
			Thread.sleep(timeintval * 1000);
		}
	}
}