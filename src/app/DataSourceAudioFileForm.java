/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.io.File;

public class DataSourceAudioFileForm extends JDialog{
    private JPanel DataSourceAudioFilePanel;

    private JPanel parameterPanel;
    private JPanel panelSamplesPerTransfer;
    private JButton selectFileButton;

    private JSpinner valueSamplesPerTransfer;
    private JTextField txtFilename;
    private JCheckBox cbLoop;

    private JTextField valueSamplesPerSecond;
    private JTextField valueChannels;
    private JTextField valueBitsPerValue;

    final DataSourceAudioFile dataSource;


    public DataSourceAudioFileForm(DataSourceAudioFile dataSource) {
        setContentPane(DataSourceAudioFilePanel);
        setModal(true);

        this.dataSource = dataSource;

        txtFilename.setText(dataSource.getFilename());
//        valueSamplesPerTransfer.setValue(dataSource.getSamples());
//        valueSamplesPerSecond.setValue(dataSource.getSamplerate());

        selectFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser(txtFilename.getText());
                FileNameExtensionFilter wavFileFilter = new FileNameExtensionFilter("WAV Audio Files","wav");
                jfc.addChoosableFileFilter(wavFileFilter);
                jfc.setFileFilter(wavFileFilter);

                int returnValue = jfc.showOpenDialog(getRootPanel());

                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    txtFilename.setText(selectedFile.getAbsolutePath());
                }
            }
        });
    }

    public boolean apply() {
//        dataSource.setSamples((Integer)(valueSamplesPerTransfer.getValue()));
//        dataSource.setSamplerate((Integer)(valueSamplesPerSecond.getValue()));
//        dataSource.setPortname((String) portsList.getSelectedItem());
        dataSource.setFilename(txtFilename.getText());
        return true;
    }

    public JPanel getRootPanel() {
        return parameterPanel;
    }

    public void setValueSamplesPerSecond(String value) {
        valueSamplesPerSecond.setText(value);
    }

    public void setValueChannels(String value) {
        valueChannels.setText(value);
    }

    public void setValueBitsPerValue(String value) {
        valueBitsPerValue.setText(value);
    }
}
