/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.prefs.Preferences;

public class WindowPersistentGeometry {
    private final Window window;
    private final Preferences preferences;
    private final String keyLocationX = "locationX";
    private final String keyLocationY = "locationY";
    private final String keyLocationWidth = "locationWidth";
    private final String keyLocationHeight = "locationHeight";

    /*
    https://stackoverflow.com/questions/22086871/how-to-detect-the-screen-position-in-a-system-with-multiple-displays
     */
    private void moveWindowToAnyScreen(Rectangle window_rect) {
        System.out.printf("GUI: window \"%s\": %s\n", window.getName() ,window_rect.toString());
        GraphicsDevice[] screens = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

        int screen_number = 0;
        int displaying_screen_number = 0;

        for (GraphicsDevice screen:screens) {
            screen_number++;
            Rectangle screen_rect = screen.getDefaultConfiguration().getBounds();
            System.out.printf("GUI: screen %d: %s\n", screen_number, screen_rect.toString());

            if (screen_rect.contains(window_rect)) {
                displaying_screen_number = screen_number;
            }
        }

        if (displaying_screen_number == 0) {
            window_rect.setLocation(0,0);
        }
    }

    public WindowPersistentGeometry(Window window, Preferences preferences, Integer x, Integer y, Integer width, Integer height) {
        this.window = window;
        this.preferences = preferences;

        int min_width = width / 8;
        int min_height = height / 8;

        Rectangle window_rect = new Rectangle();

        window_rect.width = preferences.getInt(keyLocationWidth, width);
        if (window_rect.width < min_width) {
            window_rect.width = min_width;
        }

        window_rect.height = preferences.getInt(keyLocationHeight, height);
        if (window_rect.height < min_height) {
            window_rect.height = min_height;
        }

        window_rect.x = preferences.getInt(keyLocationX, x);
        window_rect.y = preferences.getInt(keyLocationY, y);

        moveWindowToAnyScreen(window_rect);

        window.setLocation(window_rect.x, window_rect.y);
        window.setSize(window_rect.width, window_rect.height);

        window.addWindowListener(new WindowAdapter() {
            public void windowDeactivated(WindowEvent e) {
                storeWindowGeometry();
            }
        });
    }

    private void storeWindowGeometry() {
        Point location = window.getLocation();
        preferences.putInt(keyLocationX, location.x);
        preferences.putInt(keyLocationY, location.y);

        Dimension dimension = window.getSize();
        preferences.putInt(keyLocationWidth, dimension.width);
        preferences.putInt(keyLocationHeight, dimension.height);
    }
}
