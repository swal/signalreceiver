/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class DataSourceSinus extends DataSource {
    private final Preferences preferences;
    private final String keySamplerate = "samplerate";
    private final String keySamples = "samples";
    private final String keyAmplitude ="amplitude";
    private final String keyFrequency ="frequency";

    private int samplerate = 19200;
    private int samples = 5000;
    private int amplitude = 1000;
    private int frequency = 442; /* 442 Hz = Kammerton 'A' */

    private final DataSourceSinusForm dialog;
    private boolean running;


    public DataSourceSinus(Preferences preferences) {
        this.preferences = preferences;
        samplerate = (int) preferences.getLong(keySamplerate, samplerate);
        samples = (int) preferences.getLong(keySamples, samples);
        amplitude = (int) preferences.getLong(keyAmplitude, amplitude);
        frequency = (int) preferences.getLong(keyFrequency, frequency);

        dialog = new DataSourceSinusForm(this);
        running = false;
    }

    @Override
    public boolean configure(boolean showDialog) {
        if (showDialog) {
            dialog.pack();
            dialog.setVisible(true);
        } else {
            return dialog.apply();
        }
        return true;
    }

    @Override
    public JPanel getConfigPanel() {
        return dialog.getRootPanel();
    }

    @Override
    public String toString() {
        return "Sinus (Generator)";
    }

    @Override
    public boolean start() {
        running = true;
        Thread thread = new DataSourceSinusThread();
        thread.start();
        return true;
    }

    @Override
    public void stop() {
        running = false;
    }

    public int getSamplerate() {
        return samplerate;
    }

    public void setSamplerate(int samplerate) {
        this.samplerate = samplerate;
        preferences.putInt(keySamplerate, samplerate);
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
        preferences.putInt(keySamples, samples);
    }

    public int getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(int amplitude) {
        this.amplitude = amplitude;
        preferences.putInt(keyAmplitude, amplitude);
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
        preferences.putInt(keyFrequency, frequency);
    }


    private class DataSourceSinusThread extends Thread {
        @Override
        public void run() {
            double offset = 0;
            double x = 0;
            while(running) {
                List<Integer> sample_list = new ArrayList<>();

                for (int i=0; i < samples; i++) {
                    sample_list.add((int)(0.5 * amplitude * Math.sin(x) + offset));
                    x +=  2.0 * Math.PI / ((double)samplerate / (double)frequency);
                }

                DataSet dataset = new DataSet();
                dataset.sample_rate = samplerate;
                dataset.sample_list = sample_list;
                notifyData(dataset);

                try {
                    Thread.sleep((1000 + 100) * samples / samplerate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}