/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.ArrayList;
import java.util.List;

public class FFT {

    private final int n;
    private final double[] wr;
    private final double[] wi;


    public FFT(int n) {
        this.n = calculateWindowSize(n);
        System.out.printf("FFT: n=%d\n", this.n);

        this.wr = new double[2 * this.n];
        this.wi = new double[2 * this.n];

        int iteration = 0;
        for (int l = 1; l < n; l = 2*l) { /* l := 1, 2, 4, 8, ... n/2 */
            for (int m = 1; m <= l; m++) { /* m := 1, 2, 3, 4, 5 ... l */
                double a = Math.PI * (1-m)/l;
                wr[iteration] = Math.cos(a);
                wi[iteration] = Math.sin(a);
                iteration++;
            }
        }
        System.out.printf("FFT: iter=%d\n", iteration);
    }

    public static int calculateWindowSize(int n) {
        /* reduce n to be a power of two, or zero */
        int n_power_of_two = 0;
        if (n > 1)  {
            /* limit to 2^20 ~ 10°6 samples */
            for (int i = 1; i < 20; i++) {
                int onebitmask = 1 << i;
                int higherbitmask = ~((1 << (i + 1)) - 1);
                if (((n & onebitmask) != 0) && ((n & higherbitmask) == 0)) {
                    n_power_of_two = onebitmask;
                    break;
                }
            }
        }
        return n_power_of_two;
    }

    public int getN() { return this.n; }


    public List<Integer> fft(List<Integer> samples_in) {
        double[] fr = new double[n];
        double[] fi = new double[n];

        for (int i=0; i < n; i++) {
            fr[i] = samples_in.get(i);
            fi[i] = 0.0;
        }

        int mr = 0;
        int nn = n-1;

        for (int m = 1; m < nn; m++) {
            int l = n;
            do {
                l = l / 2;
            } while (mr + l > nn);
            mr = (mr % l) + l;
            if (mr > m) {
                double t;
                t = fr[m]; fr[m] = fr[mr]; fr[mr] = t;
                t = fi[m]; fi[m] = fi[mr]; fi[mr] = t;
            }
        }

        int iteration = 0;
        for (int l = 1; l < n; l = 2*l) { /* l := 1, 2, 4, 8, ... n/2 */
            for (int m = 1; m <= l; m++) { /* m := 1, 2, 3, 4, 5 ... l */
                for (int i = m-1; i < n; i = i + 2*l) {
                    int j = i + l;
                    double tr = wr[iteration] * fr[j] - wi[iteration] * fi[j];
                    double ti = wr[iteration] * fi[j] + wi[iteration] * fr[j];

                    fr[j] = fr[i] - tr;
                    fi[j] = fi[i] - ti;
                    fr[i] = fr[i] + tr;
                    fi[i] = fi[i] + ti;
                }
                iteration++;
            }
        }

        List<Integer> samples_out = new ArrayList<>();
        for (int i=0; i < n/2; i++) {
            samples_out.add((int) Math.sqrt(fr[i] * fr[i] + fi[i] * fi[i]));
        }
        return samples_out;
    }


    static FFT fft_instance;
    public static List<Integer> fft_1(List<Integer> samples_in) {
        final int n = calculateWindowSize(samples_in.size());
        if ((fft_instance == null) || (fft_instance.getN() != n)) {
            fft_instance = new FFT(n);
        }
        return fft_instance.fft(samples_in);
    }


    public static List<Integer> apply(List<Integer> samples_in) {
        final int n = samples_in.size();

        double[] fr = new double[n];
        double[] fi = new double[n];

        for (int i=0; i < n; i++) {
            fr[i] = samples_in.get(i);
            fi[i] = 0.0;
        }

        int mr = 0;
        int nn = n-1;

        for (int m = 1; m < nn; m++) {
            int l = n;
            do {
                l = l / 2;
            } while (mr + l > nn);
            mr = (mr % l) + l;
            if (mr > m) {
                double t;
                t = fr[m]; fr[m] = fr[mr]; fr[mr] = t;
                t = fi[m]; fi[m] = fi[mr]; fi[mr] = t;
            }
        }

        for (int l = 1; l < n; l = 2*l) { /* l := 1, 2, 4, 8, ... n/2 */
            for (int m = 1; m <= l; m++) { /* m := 1, 2, 3, 4, 5 ... l */
                double a = Math.PI * (1-m)/l;
                double wr = Math.cos(a);
                double wi = Math.sin(a);

                for (int i = m-1; i < n; i = i + 2*l) {
                    int j = i + l;
                    double tr = wr * fr[j] - wi * fi[j];
                    double ti = wr * fi[j] + wi * fr[j];

                    fr[j] = fr[i] - tr;
                    fi[j] = fi[i] - ti;
                    fr[i] = fr[i] + tr;
                    fi[i] = fi[i] + ti;
                }
            }
        }

        List<Integer> samples_out = new ArrayList<>();
        for (int i=0; i < n/2; i++) {
            samples_out.add((int) Math.sqrt(fr[i] * fr[i] + fi[i] * fi[i]));
        }
        return samples_out;
    }

}
