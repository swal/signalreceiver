/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.prefs.Preferences;

import static jssc.SerialPort.*;
import static jssc.SerialPort.PARITY_NONE;


public class DataSourceUartStreaming extends DataSource {
    private final Preferences preferences;
    private final String keySamplerate = "samplerate";
    private final String keySamples = "samples";
    private final String keyPortname ="portname";

    private int samplerate = 40000;
    private int samples = 20000;
    private String portname;

    private final DataSourceUartStreamingForm dialog;
    private ReceiverThread receiver_thread;
    private ParserThread parserThread;

    private boolean running;

    private SerialPort port;

    public DataSourceUartStreaming(Preferences preferences) {
        this.preferences = preferences;
        samplerate = (int) preferences.getLong(keySamplerate, samplerate);
        samples = (int) preferences.getLong(keySamples, samples);
        portname = preferences.get(keyPortname,"");

        dialog = new DataSourceUartStreamingForm(this);
        running = false;
    }

    @Override
    public String toString() {
        return "ADS1261 Streaming (UART)";
    }

    @Override
    public boolean configure(boolean showDialog) {
        if (showDialog) {
            dialog.pack();
            dialog.setVisible(true);
        } else {
            return dialog.apply();
        }
        return true;
    }

    @Override
    public JPanel getConfigPanel() {
        return dialog.getRootPanel();
    }

    @Override
    public boolean start() {
        parserThread = new ParserThread();
        parserThread.start();

        port = new SerialPort(portname);
        try {
            port.openPort();
            port.setParams(2000000, DATABITS_8, STOPBITS_1, PARITY_NONE);
        } catch (SerialPortException exception) {
            exception.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed to connect to " + portname + "\n" + exception.getExceptionType());
            return false;
        }

        running = false;
        receiver_thread = new ReceiverThread();
        try {
//            port.addEventListener(receiver_thread, MASK_RXCHAR);
            port.addEventListener(receiver_thread, 511);
            running = true;
        } catch (SerialPortException e) {
            e.printStackTrace();
        }

        return true;
    }


    @Override
    public void stop() {
        running = false;
        if (port != null) {
            try {
                port.removeEventListener();
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
            try {
                port.closePort();
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    public int getSamplerate() {
        return samplerate;
    }

    public void setSamplerate(int samplerate) {
        this.samplerate = samplerate;
        preferences.putInt(keySamplerate, samplerate);
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
        preferences.putInt(keySamples, samples);
    }

    public String getPortname() {
        return portname;
    }

    public void setPortname(String portname) {
        this.portname = portname;
        if (portname == null) {
            preferences.put(keyPortname, "");
        } else {
            preferences.put(keyPortname, portname);
        }
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String byteToHex(byte b) {
        byte[] hexChars = new byte[2];
        int v = b & 0xFF;
        hexChars[0] = (byte) HEX_ARRAY[v >>> 4];
        hexChars[1] = (byte) HEX_ARRAY[v & 0x0F];
        return new String(hexChars, StandardCharsets.UTF_8);
    }

    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = (byte) HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = (byte) HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }


    private class ParserThread extends Thread {

        boolean running;
        ArrayBlockingQueue<byte[]> in_buffer;

        ParserThread() {
            running = false;
            in_buffer = new ArrayBlockingQueue(16);
        }

        void addData(byte[] buffer) {
            try {
                in_buffer.add(buffer);
            }
            catch (IllegalStateException e) {
                System.out.println("DataSourceUartStreaming queue overflow");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        synchronized public void run() {
            System.out.println("Enter ParserThread");

            byte state = 0;
            List<Integer> sample_list = new ArrayList<>();
            int sample_value = 0;
            int sample_value_last = 0;

            byte tx_count = 0;
            byte tx_toggle = 0;

//            int read_samples = samples;

            running = true;
            while (running) {
                byte[] buffer = new byte[0];
                try {
                    buffer = in_buffer.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if ((buffer != null) && (buffer.length > 0)) {
                    /* number of bytes in "buffer" varies */
//                    System.out.println("\nRX #: " + buffer.length + "  " + bytesToHex(buffer));

//                    System.out.print(" <> ");

                    for (byte b : buffer) {
                        if (state == 0) {
                            if (((byte) (b & 0xC0)) == (byte) 0x80) {
                                boolean error = false;
//                                System.out.print("\n> ");
                                byte rx_count = (byte) (b & 0x0F);
                                tx_count = (byte) ((tx_count + 1) & 0x0F);
                                if (rx_count != tx_count) {
                                    error = true;
                                    System.out.print(" E# ");
                                    tx_count = rx_count;
                                }
                                byte rx_toggle = (byte) (b & 0x20);
                                tx_toggle = (byte) (tx_toggle ^ 0x20);
                                if (rx_toggle != tx_toggle) {
                                    error = true;
                                    System.out.print(" Et ");
                                    tx_toggle = rx_toggle;
                                }
                                if (error) {
                                    System.out.println();
                                }
                                state = 1;
                                sample_value = 0;
                            } else {
                                /* Unexpected ADC status input. Input stream not in sync. */
                                System.out.print("\n" + byteToHex(b) + "? ");
                            }
                        } else {
                            //                          System.out.print(byteToHex(b));
                            state++;
                            if (state == 2) {
//                                System.out.print('l');
                                sample_value += (b & 0xFF);
                            } else if (state == 3) {
//                                System.out.print('m');
                                sample_value += ((b & 0xFF) << 8);
                            } else if (state == 4) {
//                                System.out.print('h');
                                sample_value += (b << 16);
//                                System.out.print('!');
//                                System.out.print("  " + sample_value);

/*
                                if (sample_value != sample_value_last + 1) {
                                    System.out.println(sample_value_last + " -> " + sample_value + " d: " + (sample_value - sample_value_last));
                                }
                                sample_value_last = sample_value;
*/
                                sample_list.add(sample_value);

                                if (sample_list.size() >= samples) {
                                    DataSet dataset = new DataSet();
                                    dataset.sample_rate = samplerate;
                                    dataset.sample_list = sample_list;
                                    dataset.minValue = null;
                                    dataset.maxValue = null;
                                    dataset.meanValue = null;
                                    notifyData(dataset);

                                    sample_list = new ArrayList<>();
                                }
                                state = 0;
                            }
//                            System.out.print(' ');
                        }
                    } /* foreach byte */
                } /* buffer != null */

            } /* while running */
        } /* run() */
    }

    private class ReceiverThread implements SerialPortEventListener {
        byte[] buffer;
        public void serialEvent(SerialPortEvent event) {
            if (event.isRXCHAR()) {
                try {
//                    byte buffer[] = port.readBytes();
                    buffer = port.readBytes();
                    if ((buffer != null) && (buffer.length > 0)) {
                        parserThread.addData(buffer);
                    }
                } catch (SerialPortException ex) {
                    System.out.println(ex);
                }
            } else {
                int event_type = event.getEventType();
                if ((event_type & MASK_RXFLAG) != 0) {
                    event_type &= ~MASK_RXFLAG;
                }
                if ((event_type & MASK_ERR) != 0) {
                    event_type &= ~MASK_ERR;
                    int event_value = event.getEventValue();
                    if (event_value == ERROR_OVERRUN) {
                        System.out.println("Event ERR OVERRUN");
                    } else {
                        System.out.println("Event ERR " + event_value);
                    }
                }


                if (event_type != 0) {
                    System.out.println("event " + event_type);
                }
            }
        }
    }
}
