/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EmitterChartDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel mainPanel;

    private final XYPlot plot;
    static NumberAxis axis_ms = null;


    public EmitterChartDialog() {
        setName("Samples Time Domain");
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        JFreeChart chart = ChartFactory.createXYLineChart(null , "Samples", "Value", null, PlotOrientation.VERTICAL, false, true, false);
        chart.setAntiAlias(true);
        plot = chart.getXYPlot();
        plot.setDomainPannable(true);
        plot.setRangePannable(true);
        plot.getRenderer().setSeriesPaint(0, Color.green);
        plot.getDomainAxis().setAutoRange(true); /* "domain axis" = X-axis */
        plot.getRangeAxis().setAutoRange(true);

        Paint p = new GradientPaint(0, 0, Color.BLACK, 0, 100, Color.DARK_GRAY);
        plot.setBackgroundPaint(p);
//        plot.setBackgroundPaint(Color.DARK_GRAY);

        axis_ms = new NumberAxis("Milliseconds");
        plot.setDomainAxis(1, axis_ms);
        plot.setDomainAxisLocation(1, AxisLocation.BOTTOM_OR_LEFT);

        ChartPanel chartPanel = new ChartPanel(chart, false, false, false, true, true);
        chartPanel.setMouseWheelEnabled(true);
        mainPanel.getRootPane().setContentPane(chartPanel);

        pack();
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void notifyData(DataSet data) {
        XYSeries series = new XYSeries("VAL");
        double sampleTime_ms = 1000.0 / data.sample_rate;
        for (int i=0; i < data.sample_list.size(); i++) {
            series.add( i, data.sample_list.get(i));
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);

        plot.setDataset(dataset);

        axis_ms.setUpperBound(1000.0 * data.sample_list.size() / data.sample_rate);
/*
        plot.getRangeAxis().setRange(data.minValue, data.maxValue);
        axis_ms.setLowerBound(0);
        axis_ms.setUpperBound(sampleTime_ms * (data.sample_list.size()-1));
*/
    }
}
