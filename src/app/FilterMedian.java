/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.ArrayList;
import java.util.List;

public class FilterMedian {
    private int n = 3;

    public void setup(int n) {
        this.n = n;
    }

    public List<Integer> apply(List<Integer> samples_in) {
        List<Integer> samples_out = new ArrayList<>();
        for (int i = 0; i < samples_in.size() - n + 1; i++) {
            double median=0;
            double a = samples_in.get(i + 0);
            double b = samples_in.get(i + 1);
            double c = samples_in.get(i + 2);

            if (a < b) {
                if (b < c) {
                    median = b;
                } else if (c < a) {
                    median = a;
                } else {
                    median = c;
                }
            } else { /* a > b */
                if (c > a) {
                    median = a;
                } else if (b > c) {
                    median = b;
                } else {
                    median = c;
                }
            }
            samples_out.add((int) median);
        }
        return samples_out;
    }

}
