/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.prefs.Preferences;


public class Emitter_SignalAnalyzer extends DataListener {
    final Preferences preferences;
    final String keyActive = "active";
    final String keyFilename = "filename";

    private final String out_filename;
    private final File out_file;

    public Emitter_SignalAnalyzer(Preferences preferences) {
        this.preferences = preferences;
        setActiveEx(preferences.getBoolean(keyActive, false));
        out_filename = preferences.get(keyFilename, "output.sasx");
        out_file = new File(out_filename);
        out_file.delete();
    }

    @Override
    public void setActiveEx(boolean on)
    {
        super.setActiveEx(on);
        preferences.putBoolean(keyActive, on);
    }

    public String getFileName() { return out_filename; }

    @Override
    public void processData(DataSet data) {
        if (! isActiveEx()) {
            return;
        }

        if ((! out_file.exists()) && (data.sample_list.size() > 0)) {
            try {
                BufferedWriter writer;
                writer = new BufferedWriter(new FileWriter(out_filename));
                writer.write("Signalanalyzer\n");
                writer.write("Rate:" + data.sample_rate + "\n");
                writer.write("Unit:val\n");
                writer.write("Samples:" + data.sample_list.size() + "\n");
                for (Integer msv : data.sample_list) {
                    writer.write(msv + "\n");
                }
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
