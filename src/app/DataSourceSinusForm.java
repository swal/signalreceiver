/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.swing.*;
import java.awt.event.*;

public class DataSourceSinusForm extends JDialog {
    private JPanel DataSourceSinusPanel;

    private JButton buttonOK;
    private JButton buttonCancel;

    private JPanel parameterPanel;
    private JPanel panelSamplesPerTransfer;
    private JSpinner valueSamplesPerTransfer;
    private JPanel panelSamplesPerSecond;
    private JSpinner valueSamplesPerSecond;
    private JPanel panelFrequency;
    private JSpinner valueFrequency;
    private JPanel panelAmplitude;
    private JSpinner valueAmplitude;
    private JPanel bottomPanel;
    private JPanel buttonPanel;

    private final DataSourceSinus dataSourceSinus;

    public DataSourceSinusForm(DataSourceSinus dataSourceSinus) {
        setContentPane(DataSourceSinusPanel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        DataSourceSinusPanel.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        this.dataSourceSinus = dataSourceSinus;
        valueSamplesPerTransfer.setValue(dataSourceSinus.getSamples());
        valueSamplesPerSecond.setValue(dataSourceSinus.getSamplerate());
        valueFrequency.setValue(dataSourceSinus.getFrequency());
        valueAmplitude.setValue(dataSourceSinus.getAmplitude());
    }

    public boolean apply() {
        dataSourceSinus.setSamples((Integer)(valueSamplesPerTransfer.getValue()));
        dataSourceSinus.setSamplerate((Integer)(valueSamplesPerSecond.getValue()));
        dataSourceSinus.setFrequency((Integer)(valueFrequency.getValue()));
        dataSourceSinus.setAmplitude((Integer)(valueAmplitude.getValue()));
        return true;
    }

    private void onOK() {
        apply();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public JPanel getRootPanel() {
//        return DataSourceSinusPanel;
        return parameterPanel;
    }
}
