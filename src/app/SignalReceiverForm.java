/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import static javax.swing.JOptionPane.WARNING_MESSAGE;


public class SignalReceiverForm {
    private JPanel mainPanel;

    private JButton btnStart;
    private JButton btnStop;

    private JButton buttonConfigure;

    private JTextField textStatusSamples;
    private JTextField textStatusTotal;
    private JPanel input_panel;
    private JPanel process_panel;
    private JPanel output_panel;
    private JTextField textFinenameSignalAnalyzer;
    private JComboBox<DataSource> selectDataSource;
    private JPanel dataSourceConfigPanel;
    private JButton buttonApply;
    private JTextField textMeanValue;
    private JTextField textStandardDeviation;
    private JTextField textMinimalValue;
    private JTextField textMaximalValue;
    private JTextField textSpan;
    private JButton btnEmitterChart;
    private JPanel correction_panel;
    private JButton adjustOffsetButton;
    private JCheckBox applyOffsetCheckBox;
    private JSpinner spinnerOffset;
    private JCheckBox applyGainCheckBox;
    private JSpinner spinnerGain;
    private JRadioButton rbAverageFilter;
    private JSpinner spinnerMoovingAverage;
    private JSpinner spinnerFIRfreq;
    private JSpinner spinnerFIRorder;
    private JCheckBox highPassCheckBox;
    private JRadioButton rbFIRfilter;
    private JButton btnEmitterFFT;
    private JRadioButton rbMedianFilter;
    private JCheckBox cbAudioEnable;
    private JButton btnStartSignalAnalyzer;
    private JCheckBox cbOutputFileEnable;
//    private JButton setFilenameButton;

    private final SignalReceiver signalreceiver;
    private DataSource dataSource;

    private Emitter_SignalAnalyzer emitter_signalanalyzer;
    private DataListener emitterChart;
    private DataListener emitterFFT;

    private Double mean;

    public SignalReceiverForm(SignalReceiver signalreceiver) {
        this.signalreceiver = signalreceiver;

        btnStart.setEnabled(true);
        btnStop.setEnabled(false);

        textStatusTotal.setHorizontalAlignment(SwingConstants.RIGHT);

        textStatusSamples.setHorizontalAlignment(SwingConstants.RIGHT);
        textMinimalValue.setHorizontalAlignment(SwingConstants.RIGHT);
        textMaximalValue.setHorizontalAlignment(SwingConstants.RIGHT);
        textSpan.setHorizontalAlignment(SwingConstants.RIGHT);
        textMeanValue.setHorizontalAlignment(SwingConstants.RIGHT);
        textStandardDeviation.setHorizontalAlignment(SwingConstants.RIGHT);

        spinnerOffset.setModel(new SpinnerNumberModel(0.0, null, null, 1));
        spinnerOffset.setEditor(new JSpinner.NumberEditor(spinnerOffset,  	"###,##0.000000"));

        spinnerGain.setModel(new SpinnerNumberModel(1.0, 0.0, null, 0.01));
        spinnerGain.setEditor(new JSpinner.NumberEditor(spinnerGain,  	"###,##0.000000"));

        spinnerFIRfreq.setModel(new SpinnerNumberModel(0.25, 0.0, 0.5, 0.01));
        spinnerFIRfreq.setEditor(new JSpinner.NumberEditor(spinnerFIRfreq,  	"0.000"));

        spinnerFIRorder.setModel(new SpinnerNumberModel(2, 0, null, 1));

        spinnerMoovingAverage.setModel(new SpinnerNumberModel(1,1,null,1));

        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (dataSource != null) {
                    if (dataSource.start()) {
                        btnStart.setEnabled(false);
                        btnStop.setEnabled(true);
                        selectDataSource.setEnabled(false);
                    }
                }
            }
        });

        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (dataSource != null) {
                    dataSource.stop();
                    btnStart.setEnabled(true);
                    btnStop.setEnabled(false);
                    selectDataSource.setEnabled(true);
                }
            }
        });

        buttonConfigure.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnStart.setEnabled(false);
                dataSource = (DataSource) (selectDataSource.getSelectedItem());

                if (dataSource != null) {
/*
                    if (dataSource.configure()) {
                        btnStart.setEnabled(true);
                    }
 */
                }
            }
        });

        buttonApply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                btnStart.setEnabled(false);
                dataSource = (DataSource) (selectDataSource.getSelectedItem());

                signalreceiver.selectDataSource(dataSource);

                if (dataSource != null) {
                    if (dataSource.configure(false)) {
                        // btnStart.setEnabled(true);
                    }
                }
            }
        });

        selectDataSource.addActionListener(new ActionListener() {
            /** a different data-source was selected from the selection box */
            @Override
            public void actionPerformed(ActionEvent e) {
                /* update the card layout */
                dataSource = (DataSource) (selectDataSource.getSelectedItem());

                CardLayout cardlayout = (CardLayout)(dataSourceConfigPanel.getLayout());
                cardlayout.show(dataSourceConfigPanel, dataSource.toString());
            }
        });

        adjustOffsetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signalreceiver.addOffset(-mean);
            }
        });

        applyOffsetCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signalreceiver.enableOffset(applyOffsetCheckBox.isSelected());
            }
        });

        spinnerOffset.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                signalreceiver.setOffsetValue((Double)spinnerOffset.getValue());
            }
        });

        applyGainCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signalreceiver.enableGain(applyGainCheckBox.isSelected());
            }
        });

        spinnerGain.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                signalreceiver.setGain((Double)spinnerGain.getValue());
            }
        });

        rbFIRfilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rbFIRfilter.isSelected()) {
//                    rbFIRfilter.setSelected(false);
                    rbAverageFilter.setSelected(false);
                    rbMedianFilter.setSelected(false);

//                    signalreceiver.enableFIRfilter(false);
                    signalreceiver.enableAverageFilter(false);
                    signalreceiver.enableMedianFilter(false);
                }
                signalreceiver.enableFIRfilter(rbFIRfilter.isSelected());
            }
        });

        highPassCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (highPassCheckBox.isSelected()) {
                    signalreceiver.setFIRfilterMode(FIRfilter.Mode.HIGHPASS);
                } else {
                    signalreceiver.setFIRfilterMode(FIRfilter.Mode.LOWPASS);
                }
            }
        });

        spinnerFIRorder.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                signalreceiver.setFIRfilterOrder((Integer) spinnerFIRorder.getValue());
            }
        });

        spinnerFIRfreq.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                signalreceiver.setFIRfilterFrequency((double) spinnerFIRfreq.getValue());
            }
        });

        rbAverageFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rbAverageFilter.isSelected()) {
                    rbFIRfilter.setSelected(false);
//                    rbAverageFilter.setSelected(false);
                    rbMedianFilter.setSelected(false);

                    signalreceiver.enableFIRfilter(false);
//                    signalreceiver.enableAverageFilter(false);
                    signalreceiver.enableMedianFilter(false);
                }
                signalreceiver.enableAverageFilter(rbAverageFilter.isSelected());
            }
        });

        spinnerMoovingAverage.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                signalreceiver.setAverageFilterN((int) spinnerMoovingAverage.getValue());
            }
        });

        btnEmitterChart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                emitterChart.setActiveEx(true);
            }
        });

        btnEmitterFFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                emitterFFT.setActiveEx(true);
            }
        });

        rbMedianFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rbMedianFilter.isSelected()) {
                    rbFIRfilter.setSelected(false);
                    rbAverageFilter.setSelected(false);
//                    rbMedianFilter.setSelected(false);

                    signalreceiver.enableFIRfilter(false);
                    signalreceiver.enableAverageFilter(false);
//                    signalreceiver.enableMedianFilter(false);
                }
                signalreceiver.enableMedianFilter(rbMedianFilter.isSelected());
            }
        });

        cbAudioEnable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean enable = cbAudioEnable.isSelected();
                if (enable) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Warning: Audio will be loud!\nRemove headset and turn down volume.",
                            "Activate Audio Output",
                            WARNING_MESSAGE
                    );
                }
                signalreceiver.enableEmitterAudio(enable);
            }
        });

        btnStartSignalAnalyzer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().open(new File(emitter_signalanalyzer.getFileName()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        cbOutputFileEnable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean enable = cbOutputFileEnable.isSelected();
                signalreceiver.enableEmitterFile(enable);
            }
        });
    }

    public void addDataSource(DataSource dataSource) {
        if (dataSource != null) {
            /* add the data-source's control panel to the card layout */
            JPanel configPanel = dataSource.getConfigPanel();
            if (configPanel != null) {
                dataSourceConfigPanel.add(configPanel, dataSource.toString());
                mainPanel.updateUI();
            }
            /* add the data-source to the selection box */
            selectDataSource.addItem(dataSource);
        }
    }

    public void selectDataSource(DataSource datasource) {
        selectDataSource.setSelectedItem(datasource);
    }

    public JPanel getPanel() {
        return this.mainPanel;
    }

    public void setStatusSamples(String text) {
        textStatusSamples.setText(text);
    }

    public void setStatusAverage(Double mean) {
        this.mean = mean;
        DecimalFormat formatter = new DecimalFormat("###,##0.000000");
        textMeanValue.setText(formatter.format(mean));
    }

    public void setStatusTotal(String text) {
        textStatusTotal.setText(text);
    }

    public void setStatusStandardDeviation(String text) {
        textStandardDeviation.setText(text);
    }

    public void setStatusMinimalValue(String text) {
        textMinimalValue.setText(text);
    }

    public void setStatusMaximalValue(String text) {
        textMaximalValue.setText(text);
    }

    public void setStatusSpan(String text) {
        textSpan.setText(text);
    }


    public void addEmitterSignalAnalyter(Emitter_SignalAnalyzer emitter_signalanalyzer) {
        this.emitter_signalanalyzer = emitter_signalanalyzer;
        textFinenameSignalAnalyzer.setText(emitter_signalanalyzer.getFileName());
    }

    public void addEmitterChart(DataListener listener) {
        this.emitterChart = listener;
    }

    public void addEmitterFFT(DataListener listener) {this.emitterFFT = listener;}


    public void enableOffset(boolean isOffsetEnabled) {
        applyOffsetCheckBox.setSelected(isOffsetEnabled);
    }

    public void setOffset(Double offset) {
        spinnerOffset.setValue(offset);
    }

    public void enableGain(boolean isGainEnabled) {
        applyGainCheckBox.setSelected(isGainEnabled);
    }

    public void setGain(double gain) {
        spinnerGain.setValue(gain);
    }

    public void enableFIRfilter(boolean isFilterEnabled) {
        rbFIRfilter.setSelected(isFilterEnabled);
    }

    public void setFIRfilterFreq(double q) {
        spinnerFIRfreq.setValue(q);
    }

    public void setFIRfilterOrder(int order) {
        spinnerFIRorder.setValue(order);
    }

    public void setFIRfilterHighpass(boolean b) {
        highPassCheckBox.setSelected(b);
    }

    public void enableAveragefilter(boolean enabled) {
        rbAverageFilter.setSelected(enabled);
    }

    public void setAverageFfilterOrder(int order) {
        spinnerMoovingAverage.setValue(order);
    }

    public void enableMedianfilter(boolean enabled) {
        rbMedianFilter.setSelected(enabled);
    }
}
