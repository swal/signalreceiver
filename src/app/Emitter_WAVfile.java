/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.prefs.Preferences;


public class Emitter_WAVfile extends DataListener {
    final Preferences preferences;
    final String keyActive = "active";

    private final String out_filename = "output.wav";
    private final File out_file;

    public Emitter_WAVfile(Preferences preferences) {
        this.preferences = preferences;
        this.setActiveEx(preferences.getBoolean(keyActive, false));
        out_file = new File(out_filename);
        out_file.delete();
    }

    @Override
    public void setActiveEx(boolean on)
    {
        super.setActiveEx(on);
        preferences.putBoolean(keyActive, on);
    }

    @Override
    public void processData(DataSet data) {
        if (! isActiveEx()) {
            return;
        }

        if ((! out_file.exists()) && (data.sample_list.size() > 0)) {
            /*
             * https://docs.fileformat.com/audio/wav/
             */

            int filesize_4 = 36 /* + roundup(length_2) */;
            int length_2 = 36;
            short channels = 1;
            int samplerate = data.sample_rate;
            int bits_per_sample = 32;

            ByteBuffer b = ByteBuffer.allocate(44);
            b.order(ByteOrder.BIG_ENDIAN);

            /* Example: CLICK.WAV, 80 Bytes
             *
             * Offset(h) 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F
             *
             * 00000000  52 49 46 46 48 00 00 00 57 41 56 45 66 6D 74 20  RIFFH...WAVEfmt
             * 00000010  10 00 00 00 01 00 01 00 11 2B 00 00 11 2B 00 00  .........+...+..
             * 00000020  01 00 08 00 64 61 74 61 23 00 00 00 80 FA 0A FF  ....data#...€ú.ÿ
             * 00000030  06 E0 7D 9D A5 94 8F 9E 7F 4B 84 6D 9C 7A 90 5A  .à}.¥”.ž.K„mœz.Z
             * 00000040  86 5E 82 6F 82 71 81 6B 82 72 7E 74 75 70 74 00  †^‚o‚q.k‚r~tupt.
             *
             * Interpreted:
             *
             * 52 49 46 46   RIFF    1- 4  “RIFF” - Marks the file as a riff file. Characters are each 1 byte long.
             * 48 00 00 00   H...    5- 8  72 - File size (integer) - Size of the overall file - 8 bytes, in bytes (32-bit integer). Typically, you’d fill this in after creation.
             * 57 41 56 45   WAVE    9-12  “WAVE” - File Type Header. For our purposes, it always equals “WAVE”.
             * 66 6D 74 20   fmt    13-16  “fmt " - Format chunk marker. Includes trailing null
             * 10 00 00 00   ....   17-20  16 - Length of format data as listed above
             * 01 00         ..     21-22  1 - Type of format (1 is PCM) - 2 byte integer
             * 01 00         ..     23-24  1 - Number of Channels - 2 byte integer
             * 11 2B 00 00   .+..   25-28  11025 - Sample Rate - 32 byte integer. Common values are 44100 (CD), 48000 (DAT). Sample Rate = Number of Samples per second, or Hertz.
             * 11 2B 00 00   .+..   29-32  11025 - (Sample Rate * BitsPerSample * Channels) / 8.
             * 01 00         ..     33-34  1 - (BitsPerSample * Channels) / 8.  1 = 8 bit mono  2 = 8 bit stereo/16 bit mono  4 = 16 bit stereo
             * 08 00         ..     35-36  8 - Bits per sample
             * 64 61 74 61   data   37-40  “data” - “data” chunk header. Marks the beginning of the data section.
             * 23 00 00 00   #...   41-44  35  File size (data) - Size of the data section.
             *
             * 80 FA 0A FF   €ú.ÿ   1
             * 06 E0 7D 9D   .à}.   2
             * A5 94 8F 9E   ¥”.ž   3
             * 7F 4B 84 6D   .K„m   4
             * 9C 7A 90 5A   œz.Z   5
             * 86 5E 82 6F   †^‚o   6
             *
             * add one 00-byte to make length even.
             * */

            b.put(      0, "RIFF".getBytes());
            b.putInt(   4, filesize_4);
            b.put(      8, "WAVE".getBytes());
            b.put(     12, "fmt ".getBytes());
            b.putInt(  16, 16);
            b.putShort(20, (short) 1); /* 1: PCM */
            b.putShort(22, channels);
            b.putInt(  24, samplerate);
            b.putInt(  28, samplerate * bits_per_sample * channels / 8);
            b.putShort(32, (short) (bits_per_sample * channels / 8));
            b.putShort(34, (short) bits_per_sample);
            b.put(     36, "data".getBytes());
            b.putInt(  40, length_2);

            try {
                FileChannel filechannel = new FileOutputStream(out_file, false).getChannel();
                filechannel.write(b);

                for (Integer msv : data.sample_list) {
                    ByteBuffer bSample = ByteBuffer.allocate(4);
                    bSample.putInt(msv);
                    filechannel.write(bSample);
                }
                filechannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
