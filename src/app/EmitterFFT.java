/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.prefs.Preferences;

public class EmitterFFT extends DataListener {
    private final Preferences preferences;

    final EmitterFFTDialog dialog;
    private final WindowPersistentGeometry windowgeometry;

    public EmitterFFT(Preferences preferences) {
        this.preferences = preferences;
        dialog = new EmitterFFTDialog();
        windowgeometry = new WindowPersistentGeometry(dialog, preferences, 500, 0, 500, 350);
    }

    @Override
    public void setActiveEx(boolean active) {
        super.setActiveEx(active);
        if (active) {
            dialog.setVisible(true);
        }
    }

    @Override
    public void processData(DataSet data) {
        if (! isActiveEx()) {
            return;
        }

        DataSet new_data = new DataSet();
        new_data.minValue = 0;
        new_data.sample_rate = data.sample_rate;
        new_data.sample_list = FFT.fft_1(data.sample_list);

        dialog.notifyData(new_data);
    }
}
