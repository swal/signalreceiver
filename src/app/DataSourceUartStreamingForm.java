/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import jssc.SerialPortList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class DataSourceUartStreamingForm extends JDialog {
    private JPanel DataSourceADS1261Panel;

    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel bottomPanel;
    private JPanel buttonPanel;
    private JPanel parameterPanel;
    private JButton btnScanPorts;
    private JComboBox<String> portsList;
    private JSpinner valueSamplesPerSecond;
    private JSpinner valueSamplesPerTransfer;
    private JPanel panelSamples;
    private JPanel panelSamplesPerSecond;
    private JPanel panelPort;

    final DataSourceUartStreaming dataSourceUartStreaming;


    private class UARTPortScanner extends SwingWorker<String, Void> {

        @Override
        protected String doInBackground() throws Exception {
            scanUartPorts();
            parameterPanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            return null;
        }
    }


    public DataSourceUartStreamingForm(DataSourceUartStreaming dataSourceUartStreaming) {
        setContentPane(DataSourceADS1261Panel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
/*
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
*/
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        DataSourceADS1261Panel.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        this.dataSourceUartStreaming = dataSourceUartStreaming;
        valueSamplesPerTransfer.setValue(dataSourceUartStreaming.getSamples());
        valueSamplesPerSecond.setValue(dataSourceUartStreaming.getSamplerate());

        btnScanPorts.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parameterPanel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                UARTPortScanner scanner =  new UARTPortScanner();
                scanner.execute();
            }
        });
    }

    private void scanUartPorts() {
        portsList.removeAllItems();
        System.out.println("Scanning for COM ports");
        if (SerialPortList.getPortNames().length <= 0) {
            System.out.println("No COM ports detected");
            JOptionPane.showMessageDialog(null, "No COM ports detected");
        } else {
            System.out.print("COM ports:");
            for (String portname : SerialPortList.getPortNames()) {
                System.out.print(" ");
                System.out.print(portname);
                portsList.addItem(portname);
            }
            System.out.println(".");
        }
    }

    private void createUIComponents() {
        valueSamplesPerTransfer = new JSpinner();
        valueSamplesPerTransfer.setModel(new SpinnerNumberModel(8000, 0, 16000, 1));
    }

    public boolean apply() {
        dataSourceUartStreaming.setSamples((Integer)(valueSamplesPerTransfer.getValue()));
        dataSourceUartStreaming.setSamplerate((Integer)(valueSamplesPerSecond.getValue()));
        dataSourceUartStreaming.setPortname((String) portsList.getSelectedItem());
        return true;
    }

    private void onOK() {
        apply();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public JPanel getRootPanel() {
//        return DataSourceADS1261Panel;
        return parameterPanel;
    }
}
