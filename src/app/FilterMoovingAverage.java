/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.ArrayList;
import java.util.List;

public class FilterMoovingAverage {
    private int n = 1;

    public void setup(int n) {
        this.n = n;
    }

    public List<Integer> apply(List<Integer> samples_in) {
        List<Integer> samples_out = new ArrayList<>();
        for (int i = 0; i < samples_in.size() - n; i++) {
            double new_value = 0;
            for (int k = 0; k < n; k++) {
                new_value += samples_in.get(i + k);
            }
            new_value = new_value / n;
            samples_out.add((int) new_value);
        }
        return samples_out;
    }
}
