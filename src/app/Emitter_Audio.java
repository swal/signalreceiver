/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

/*
 * https://www.codejava.net/coding/how-to-play-back-audio-in-java-with-examples
 * https://stackoverflow.com/questions/3297749/java-reading-manipulating-and-writing-wav-files
 *
 * http://jsresources.sourceforge.net/faq_audio.html
 */

import javax.sound.sampled.*;
import java.util.concurrent.ArrayBlockingQueue;

public class Emitter_Audio extends DataListener {
    AudioFormat audioFormat;
    DataLine.Info info;
    SourceDataLine sourceDataLine;
    LineListener linelistener;

    public Emitter_Audio() {
    }

    public void processData(DataSet data) {
        if (! isActiveEx()) {
            audioFormat = null;
            return;
        }

        if (audioFormat == null) {
            audioFormat = new AudioFormat(
//                AudioFormat.Encoding.PCM_SIGNED, 20000, 8, 1, 4, 20000, false);
//                AudioFormat.Encoding.PCM_SIGNED, 44100, 16, 2, 4, 44100, false);
                    data.sample_rate,8, 1, true,false);
            info = new DataLine.Info(SourceDataLine.class, audioFormat);
            try {
                sourceDataLine = (SourceDataLine)AudioSystem.getLine(info);
                sourceDataLine.open(audioFormat);
                System.out.printf("Emitter_Audio: buffer_size = %d\n", sourceDataLine.getBufferSize());
                sourceDataLine.addLineListener(new LineListener() {
                    @Override
                    public void update(LineEvent event) {
                        System.out.println(event);
                    }
                });
                sourceDataLine.start();
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }
        }


        int num_samples = data.sample_list.size();
        int num_bytes = num_samples;

        byte[] byte_buffer = new byte[num_bytes];

        int scale = (int) Math.max(data.maxValue - data.meanValue, data.meanValue - data.minValue);

        for (int i=0; i < num_samples; i++ ) {
            Integer i_value = data.sample_list.get(i);
            byte_buffer[i] = (byte) ((i_value - data.meanValue)* 126 / scale);
        }

//        sourceDataLine.start();

        // The write operation blocks while the system plays the sound.
        sourceDataLine.write(byte_buffer, 0, num_bytes);
        sourceDataLine.write(byte_buffer, 0, 0);

//        sourceDataLine.drain();
//        sourceDataLine.stop();

//        sourceDataLine.close();
//        sourceDataLine.flush();
    }
}
