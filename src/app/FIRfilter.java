/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.ArrayList;
import java.util.List;

public class FIRfilter {
    public enum Mode {
        LOWPASS,
        HIGHPASS
    }

    private Integer N;
    private double[] coefficients;

    private static double si(double x) {
        if (x == 0) return 1;
        return Math.sin(x) / x;
    }

    public void setup(double sample_frequency, double cutoff_frequency, int degree, Mode mode) {
        N = 2 * degree + 1;
        System.out.printf("N=%d\n", N);
        coefficients = new double[N];
        for (int k = 0; k < degree; k++) {
            double ak = 2 * cutoff_frequency / sample_frequency * si(k * 2 * Math.PI * cutoff_frequency / sample_frequency);

            coefficients[degree + k] = ak;
            coefficients[degree - k] = ak;
        }
        if (mode == Mode.HIGHPASS) {
            for (int i = 0; i < N; i++) {
                if (i == degree) {
                    coefficients[i] = 1.0 - coefficients[i];
                } else {
                    coefficients[i] = 0.0 - coefficients[i];
                }
            }
        }
        for (int k = 0; k < coefficients.length; k++) {
            System.out.printf("ak[%2d] = %f\n", k, coefficients[k]);
        }
    }

    public List<Integer> apply(List<Integer> samples_in) {
        List<Integer> samples_out = new ArrayList<>();
        for (int i=0; i<samples_in.size() - N ; i++) {
            double new_value = 0;
            for (int k = 0; k < N; k++) {
                new_value += coefficients[k] * samples_in.get(i + k);
            }
            samples_out.add((int)new_value);
        }
        return samples_out;
    }
}
