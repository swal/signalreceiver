/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;


public class SignalReceiver extends DataListener {
    private final Preferences preferences;
    private final static String preferencesFileName = "SignalReceiver.preferences";

    private final String keyActiveDatasource = "datasource.active";

    private final String keyOffsetEnable = "offset.enable";
    private final String keyOffsetValue = "offset.value";
    private final String keyGainEnable = "gain.enable";
    private final String keyGainValue = "gain.value";

    private final String keyFilterFIRenable = "filterFIR.enable";
    private final String keyFilterFIRfreq = "filterFIR.freq";
    private final String keyFilterFIRorder = "filterFIR.order";
    private final String keyFilterFIRisHighpass = "filterFIR.highpass";

    private final String keyFilterAverageEnable = "filterAverage.enable";
    private final String keyFilterAverageOrder = "filterAverage.order";

    private final String keyFilterMedianEnable = "filterMedian.enable";
    private final Emitter_Audio emitter_audio;
    private final Emitter_File emitter_file;

    private boolean isOffsetEnabled;
    private double offsetValue;
    private boolean isGainEnabled;
    private double gainFactor;


    private final FIRfilter fir_filter;
    private boolean isFIRfilterEnabled;
    private double fir_filter_freq;
    private int fir_filter_degree;
    private FIRfilter.Mode fir_filter_mode;

    private final FilterMoovingAverage average_filter;
    private boolean isAverageFilterEnabled;
    private int average_filter_order;

    private final FilterMedian median_filter;
    private boolean isMedianFilterEnabled;


    private static void loadPreferences() {
        try (FileInputStream fis = new FileInputStream(preferencesFileName)) {
            Preferences.importPreferences(fis);
        }
        catch(FileNotFoundException ex) {
            System.out.printf("Warning: Application preferences file \"%s\" not found, using defaults.\n", preferencesFileName);
        }
        catch (IOException | InvalidPreferencesFormatException ex) {
            ex.printStackTrace();
        }
    }

    private static void storePreferences() {
        Preferences preferences = getApplicationPreferences();
        try (FileOutputStream fos = new FileOutputStream(preferencesFileName)) {
            preferences.exportSubtree(fos);
        } catch (IOException | BackingStoreException ex) {
            ex.printStackTrace();
        }
    }

    public static Preferences getApplicationPreferences() {
        Preferences preferencesUserRoot = Preferences.userRoot();
        Preferences preferencesApplications = preferencesUserRoot.node("applications");
        return preferencesApplications.node("SignalReceiver");
    }


    private final WindowPersistentGeometry windowgeometry;

    private final SignalReceiverForm form;

    private final List<DataSource> datasourceList;

    private final LinkedList<DataListener> datalistener = new LinkedList<>();

    private Integer total;

    public SignalReceiver() {
        preferences = getApplicationPreferences();
        total = 0;

        JFrame frame = new JFrame("SignalReceiver");
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                storePreferences();
            }
        });

        form = new SignalReceiverForm(this);

        datasourceList = new ArrayList<>();
        datasourceList.add(new DataSourceIOLPCBA(preferences.node("DataSourceIOLPCBA")));
        datasourceList.add(new DataSourceSinus(preferences.node("DataSourceSinus")));
        datasourceList.add(new DataSourceNoise(preferences.node("DataSourceNoise")));
        datasourceList.add(new DataSourceAudioFile(preferences.node("DataSourceAudioFile")));
        datasourceList.add(new DataSourceUartStreaming(preferences.node("DataSourceUartStreaming")));
        datasourceList.add(new DataSourceUartText(preferences.node("DataSourceUartText")));

        for (DataSource datasource: datasourceList) {
            datasource.setDataListener(this);
            form.addDataSource(datasource);
        }

        String active_datasource = preferences.get(keyActiveDatasource, "");
        for (DataSource datasource: datasourceList) {
            if (active_datasource.equals(datasource.toString())) {
                form.selectDataSource(datasource);
            }
        }

        
        Emitter_SignalAnalyzer emitter_signalanalyzer = new Emitter_SignalAnalyzer(preferences.node("Emitter_SignalAnalyzer"));
        emitter_signalanalyzer.setActiveEx(true);
        datalistener.add(emitter_signalanalyzer);
        form.addEmitterSignalAnalyter(emitter_signalanalyzer);

        EmitterChart emitter_chart = new EmitterChart(preferences.node("Emitter_Chart"));
        emitter_chart.setActiveEx(false);
        datalistener.add(emitter_chart);
        form.addEmitterChart(emitter_chart);

        EmitterFFT emitter_fft = new EmitterFFT(preferences.node("Emitter_FFT"));
        emitter_fft.setActiveEx(false);
        datalistener.add(emitter_fft);
        form.addEmitterFFT(emitter_fft);

        emitter_file = new Emitter_File(preferences.node("Emitter_File"));
        emitter_file.setActiveEx(false);
        datalistener.add(emitter_file);

        emitter_audio = new Emitter_Audio();
        emitter_audio.setActiveEx(false);
        datalistener.add(emitter_audio);

        
        frame.setContentPane(form.getPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        windowgeometry = new WindowPersistentGeometry(frame, preferences, 0, 300, 600, 450);

        isOffsetEnabled = preferences.getBoolean(keyOffsetEnable,false);
        offsetValue = preferences.getDouble(keyOffsetValue,0.0);
        isGainEnabled = preferences.getBoolean(keyGainEnable,false);
        gainFactor = preferences.getDouble(keyGainValue,1.0);
        form.enableOffset(isOffsetEnabled);
        form.setOffset(offsetValue);
        form.enableGain(isGainEnabled);
        form.setGain(gainFactor);

        fir_filter = new FIRfilter();
        isFIRfilterEnabled = preferences.getBoolean(keyFilterFIRenable,false);
        fir_filter_freq = preferences.getDouble(keyFilterFIRfreq,0.25);
        fir_filter_degree = preferences.getInt(keyFilterFIRorder,6);
        if (preferences.getBoolean(keyFilterFIRisHighpass, false)) {
            fir_filter_mode = FIRfilter.Mode.HIGHPASS;
        } else {
            fir_filter_mode = FIRfilter.Mode.LOWPASS;
        }
        setupFIRfilter();
        form.enableFIRfilter(isFIRfilterEnabled);
        form.setFIRfilterFreq(fir_filter_freq);
        form.setFIRfilterOrder(fir_filter_degree);
        form.setFIRfilterHighpass(fir_filter_mode == FIRfilter.Mode.HIGHPASS);

        average_filter = new FilterMoovingAverage();
        isAverageFilterEnabled = preferences.getBoolean(keyFilterAverageEnable, false);
        average_filter_order = preferences.getInt(keyFilterAverageOrder, 16);
        average_filter.setup(average_filter_order);
        form.enableAveragefilter(isAverageFilterEnabled);
        form.setAverageFfilterOrder(average_filter_order);

        median_filter = new FilterMedian();
        isMedianFilterEnabled = preferences.getBoolean(keyFilterMedianEnable, false);
        form.enableMedianfilter(isMedianFilterEnabled);

    }

    private void setupFIRfilter() {
        fir_filter.setup(1, fir_filter_freq, fir_filter_degree, fir_filter_mode);
    }

    @Override
    public void setActiveEx(boolean on) {
    }

    @Override
    public boolean isActiveEx() {
        return false;
    }

    public List<Integer> correction_linear(List<Integer> input_values) {
        List<Integer> output_values = new ArrayList<>();

        for (double value : input_values) {
            if (isOffsetEnabled) {
                value += offsetValue;
            }
            if (isGainEnabled) {
                value *= gainFactor;
            }
            output_values.add((int) value);
        }
        return output_values;
    }

    synchronized void processData(DataSet new_data) {
        new_data.sample_list = correction_linear(new_data.sample_list);

        if (isFIRfilterEnabled) {
            new_data.sample_list = fir_filter.apply(new_data.sample_list);
        }
        if (isAverageFilterEnabled) {
            new_data.sample_list = average_filter.apply(new_data.sample_list);
        }
        if (isMedianFilterEnabled) {
            new_data.sample_list = median_filter.apply(new_data.sample_list);
        }

        int n = new_data.sample_list.size();
        total += n;

        long min_value = new_data.sample_list.get(0);
        long max_value = new_data.sample_list.get(0);
        long span;
        long sum_of_values = 0;
        long sum_of_squares = 0;

        for (Integer i: new_data.sample_list ) {
            long l = i;

            if (l < min_value) {
                min_value = l;
            }
            if (l > max_value) {
                max_value = l;
            }
            sum_of_values += l;
            sum_of_squares += (l * l);
        }
        span = max_value - min_value;
        double mean = (double)sum_of_values / n;
        double standard_deviation = Math.sqrt((sum_of_squares - n * mean * mean) / (n - 1));
        new_data.minValue = (int)min_value;
        new_data.maxValue = (int)max_value;
        new_data.meanValue = mean;

        if (form != null) {
            form.setStatusTotal(String.valueOf(total));
            form.setStatusSamples(String.valueOf(n));
            form.setStatusMinimalValue(String.valueOf(min_value));
            form.setStatusMaximalValue(String.valueOf(max_value));
            form.setStatusSpan(String.valueOf(span));
            form.setStatusAverage(mean);
            form.setStatusStandardDeviation(String.format("%.6f", standard_deviation));
        }


        for (DataListener listener : datalistener) {
            if (listener.isActiveEx()) {
                listener.notifyData(new_data);
            }
        }
    }


    public void enableOffset(boolean enable) {
        isOffsetEnabled = enable;
        preferences.putBoolean(keyOffsetEnable, isOffsetEnabled);
    }

    public void addOffset(double value) {
        offsetValue += value;
        form.setOffset(offsetValue);
        preferences.putDouble(keyOffsetValue, offsetValue);
    }

    public void setOffsetValue(Double value) {
        offsetValue = value;
        preferences.putDouble(keyOffsetValue, offsetValue);
    }


    public void enableGain(boolean enable) {
        isGainEnabled = enable;
        preferences.putBoolean(keyGainEnable, isGainEnabled);
    }

    public void setGain(Double value) {
        gainFactor = value;
        preferences.putDouble(keyGainValue, gainFactor);
    }

    public void enableFIRfilter(boolean enable) {
        isFIRfilterEnabled = enable;
        preferences.putBoolean(keyFilterFIRenable, isFIRfilterEnabled);
    }

    public void setFIRfilterMode(FIRfilter.Mode mode) {
        fir_filter_mode = mode;
        setupFIRfilter();
        preferences.putBoolean(keyFilterFIRisHighpass, fir_filter_mode == FIRfilter.Mode.HIGHPASS);
    }

    public void setFIRfilterOrder(Integer value) {
        fir_filter_degree = value;
        setupFIRfilter();
        preferences.putInt(keyFilterFIRorder, fir_filter_degree);
    }

    public void setFIRfilterFrequency(double value) {
        fir_filter_freq = value;
        setupFIRfilter();
        preferences.putDouble(keyFilterFIRfreq, fir_filter_freq);
    }

    public void enableAverageFilter(boolean enable) {
        isAverageFilterEnabled = enable;
        preferences.putBoolean(keyFilterAverageEnable, isAverageFilterEnabled);
    }

    public void setAverageFilterN(Integer n) {
        average_filter_order = n;
        average_filter.setup(n);
        preferences.putInt(keyFilterAverageOrder, average_filter_order);
    }

    public void selectDataSource(DataSource dataSource) {
        preferences.put(keyActiveDatasource, dataSource.toString());
    }

    public void enableMedianFilter(boolean enable) {
        isMedianFilterEnabled = enable;
        preferences.putBoolean(keyFilterMedianEnable, isMedianFilterEnabled);
    }

    public void enableEmitterFile(boolean enabled) {
        emitter_file.setActiveEx(enabled);
    }

    public void enableEmitterAudio(boolean enabled) {
        emitter_audio.setActiveEx(enabled);
    }

    public static void main(String[] args) {
        loadPreferences();
        new SignalReceiver();
    }
}
