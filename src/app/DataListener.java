/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.concurrent.ArrayBlockingQueue;

public abstract class DataListener {
    private boolean active;
    ArrayBlockingQueue<DataSet> in_buffer;

    public DataListener() {
        active = false;
        in_buffer = new ArrayBlockingQueue<>(4);
        Thread thread = new DataProcessingThread();
        thread.start();

    }

    void setActiveEx(boolean active) {
        this.active = active;
    }

    boolean isActiveEx() {
        return active;
    }

    abstract void processData(DataSet data);

    void notifyData(DataSet data) {
        try {
            in_buffer.add(data);
        } catch (IllegalStateException e) {
            System.out.println(getClass().getName() + " queue overflow");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DataProcessingThread extends Thread {
        @Override
        synchronized public void run() {
            try {
                while (true) {
                    DataSet new_data = in_buffer.take();
                    processData(new_data);
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
