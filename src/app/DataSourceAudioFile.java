/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class DataSourceAudioFile extends DataSource {
    private final Preferences preferences;
    private final String keySamples = "samples";
    private final String keyFilename = "filename";
    private final String keyLoop = "loop";


    private int samples = 5000;
    private String filename;
    private final boolean loop;

    AudioInputStream audioInputStream;
    AudioFormat audioFormat;


    private final DataSourceAudioFileForm dialog;
    private boolean running;


    public DataSourceAudioFile(Preferences preferences) {
        this.preferences = preferences;
        samples = preferences.getInt(keySamples, samples);
        filename = preferences.get(keyFilename, "N:\\audio\\misc\\Akustische Signale\\Der Microsoft-Sound.wav");
        loop = preferences.getBoolean(keyLoop, false);

        dialog = new DataSourceAudioFileForm(this);
//        openAudioFile();
        running = false;
    }

    @Override
    public boolean configure(boolean showDialog) {
        if (showDialog) {
            dialog.pack();
            dialog.setVisible(true);
        } else {
            return dialog.apply();
        }
        return true;
    }

    @Override
    public JPanel getConfigPanel() {
        return dialog.getRootPanel();
    }

    @Override
    public String toString() {
        return "Audio File";
    }

    @Override
    public boolean start() {
        running = true;
        Thread thread = new DataSourceAudioFileThread();
        thread.start();
        return true;
    }

    @Override
    public void stop() {
        running = false;
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
        preferences.putInt(keySamples, samples);
    }

    private boolean openAudioFile() {
        audioInputStream = null;
        audioFormat = null;

        File fileIn = new File(filename);
        try {
            audioInputStream = AudioSystem.getAudioInputStream(fileIn);
            audioFormat = audioInputStream.getFormat();
            System.out.printf("AudioFile: %s\n", filename);
            System.out.printf("AudioFile: %s\n", audioFormat.toString());
            System.out.printf("AudioFile: channels   = %d\n", audioFormat.getChannels());
            System.out.printf("AudioFile: framesize  = %d\n", audioFormat.getFrameSize());
            System.out.printf("AudioFile: framerate  = %f\n", audioFormat.getFrameRate());
            System.out.printf("AudioFile: samplerate = %f\n", audioFormat.getSampleRate());
            System.out.printf("AudioFile: bit/sample = %d\n", audioFormat.getSampleSizeInBits());

            dialog.setValueSamplesPerSecond(Integer.toString((int) audioFormat.getSampleRate()));
            dialog.setValueChannels(Integer.toString(audioFormat.getChannels()));
            dialog.setValueBitsPerValue(Integer.toString(audioFormat.getSampleSizeInBits()));
        } catch (UnsupportedAudioFileException e) {
            JOptionPane.showMessageDialog(null, "Error: Failed to parse audio file");
            return false;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error: Audio file not found");
            return false;
        }
        return true;
    }

    public void setFilename(String filename) {
        this.filename = filename;
        preferences.put(keyFilename, filename);
        openAudioFile();
    }

    public String getFilename() {
        return this.filename;
    }


    private class DataSourceAudioFileThread extends Thread {
        @Override
        public void run() {

            int totalFramesRead = 0;
            try {
                int samplerate = 19200;

                samplerate = (int) (audioFormat.getSampleRate());

                int bytesPerFrame = audioFormat.getFrameSize();
                if (bytesPerFrame == AudioSystem.NOT_SPECIFIED) {
                    // some audio formats may have unspecified frame size
                    // in that case we may read any amount of bytes
                    bytesPerFrame = 1;
                }

                // Set an arbitrary buffer size of 1024 frames.
                int numBytes = bytesPerFrame * samples;

                byte[] audioBytes = new byte[numBytes];
                try {
                    int numBytesRead = 0;
                    int numFramesRead = 0;
                    // Try to read numBytes bytes from the file.
                    while (running && ((numBytesRead = audioInputStream.read(audioBytes))) != -1) {
                        // Calculate the number of frames actually read.
                        numFramesRead = numBytesRead / bytesPerFrame;
                        totalFramesRead += numFramesRead;
                        // Here, do something useful with the audio data that's
                        // now in the audioBytes array...
                        List<Integer> sample_list = new ArrayList<>();

                        int frame_index = 0;
//                        System.out.printf("AudioFile: %d bit/sample, stride = %d byte\n", audioFormat.getSampleSizeInBits(), bytesPerFrame);

                        ByteArrayConvert.ByteOrder byteorder;
                        if (audioFormat.isBigEndian()) {
                            byteorder = ByteArrayConvert.ByteOrder.BIG_ENDIAN;
                        } else {
                            byteorder = ByteArrayConvert.ByteOrder.LITTLE_ENDIAN;
                        }

                        boolean signed = true;
                        AudioFormat.Encoding encoding = audioFormat.getEncoding();
                        if (encoding == AudioFormat.Encoding.PCM_SIGNED) {
                            signed = true;
                        } else if(encoding == AudioFormat.Encoding.PCM_UNSIGNED) {
                            signed = false;
                        }

                        switch (audioFormat.getSampleSizeInBits()) {
                            case 8: {
                                for (int i = 0; i < numFramesRead; i++) {
                                    int value = ByteArrayConvert.toInteger(audioBytes, frame_index, 1, byteorder, signed);
                                    sample_list.add(value);
                                    frame_index += bytesPerFrame;
                                }
                            }
                            break;
                            case 16: {
                                for (int i = 0; i < numFramesRead; i++) {
                                    int value = ByteArrayConvert.toInteger(audioBytes, frame_index, 2, byteorder, signed);
//                                    System.out.printf("%d: %d\n", i, value);
                                    sample_list.add(value);
                                    frame_index += bytesPerFrame;
                                }
                            }
                            break;
                            case 24: {
                                for (int i = 0; i < numFramesRead; i++) {
                                    int value = ByteArrayConvert.toInteger(audioBytes, frame_index, 3, byteorder, signed);
//                                    System.out.printf("%d: %d\n", i, value);
                                    sample_list.add(value);
                                    frame_index += bytesPerFrame;
                                }
                            }
                            break;
                            case 32: {
                                for (int i = 0; i < numFramesRead; i++) {
                                    int value = ByteArrayConvert.toInteger(audioBytes, frame_index, 4, byteorder, signed);
//                                    System.out.printf("%d: %d\n", i, value);
                                    sample_list.add(value);
                                    frame_index += bytesPerFrame;
                                }
                            }
                            break;
                            default:
                                System.out.printf("AudioFile: bit/sample = %d not implemented\n", audioFormat.getSampleSizeInBits());
                                break;
                        }
                        DataSet dataset = new DataSet();
                        dataset.sample_rate = samplerate;
                        dataset.sample_list = sample_list;
                        notifyData(dataset);

                        try {
                            Thread.sleep((1000 + 100) * samples / samplerate);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception ex) {
                    // Handle the error...
                }
            } catch (Exception e) {
                // Handle the error...
            }


            while(running) {
            }
        }
    }
}
