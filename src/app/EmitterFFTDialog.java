/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EmitterFFTDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel mainPanel;

    private final XYPlot plot;


    public EmitterFFTDialog() {
        setName("FFT of Signal");
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        JFreeChart chart = ChartFactory.createXYLineChart(null , "Frequency in Hz", "", null, PlotOrientation.VERTICAL, false, true, false);
        chart.setAntiAlias(true);
        plot = chart.getXYPlot();
        plot.setDomainPannable(true);
        plot.setRangePannable(true);
        plot.getRenderer().setSeriesPaint(0, Color.green);
        plot.getDomainAxis().setAutoRange(true); /* "domain axis" = X-axis */
        plot.getRangeAxis().setAutoRange(true);

        Paint p = new GradientPaint(0, 100, Color.BLACK, 0, 0, Color.DARK_GRAY);
        plot.setBackgroundPaint(p);
//        plot.setBackgroundPaint(Color.DARK_GRAY);

        ChartPanel chartPanel = new ChartPanel(chart, false, false, false, true, true);
        chartPanel.setMouseWheelEnabled(true);
        mainPanel.getRootPane().setContentPane(chartPanel);

        pack();
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void notifyData(DataSet data) {
        XYSeries series = new XYSeries("VAL");
        double sampleHz = data.sample_rate / 2.0 / data.sample_list.size();
        for (int i=0; i < data.sample_list.size(); i++) {
            series.add( sampleHz * i, data.sample_list.get(i));
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);

        plot.setDataset(dataset);
/*
        if (data.minValue != null) {
            plot.getRangeAxis().setLowerBound(data.minValue);
        }
        if ((data.minValue != null) && (data.maxValue != null)) {
            plot.getRangeAxis().setRange(data.minValue, data.maxValue);
        }
 */
    }
}
