/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.List;

public class DataSet {
    public int sample_rate = 0;
    public List<Integer> sample_list;
    public Integer minValue;
    public Integer maxValue;
    public Double meanValue;
}
