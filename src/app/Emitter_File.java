/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.prefs.Preferences;

public class Emitter_File extends DataListener {
    final Preferences preferences;
    final String keyFilename = "filename";

    private final String out_filename;
    BufferedWriter writer;

    public Emitter_File(Preferences preferences) {
        this.preferences = preferences;
        out_filename = preferences.get(keyFilename, "output.txt");
    }

    @Override
    public void setActiveEx(boolean on)
    {
        if (!isActiveEx() && on) {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            String dateString = dateFormat.format(date);
            String series_filename;
            int indexOfDot = out_filename.lastIndexOf('.');
            if (indexOfDot < 0) {
                series_filename =  out_filename + "_" + dateString;
            } else if (indexOfDot == 0) {
                series_filename =  dateString + out_filename;
            } else {
                series_filename =  out_filename.substring(0, indexOfDot) + "_" + dateString + out_filename.substring(indexOfDot);
            }

            File out_file = new File(series_filename);

            if (out_file.exists()) {
            } else {
                try {
                    writer = new BufferedWriter(new FileWriter(series_filename));
                    System.out.println("Output File: " + series_filename);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (isActiveEx() && !on) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            writer = null;
        }
        super.setActiveEx(on);
    }

    @Override
    public void processData(DataSet data) {
        if (! isActiveEx()) {
            return;
        }
        if (writer != null) {
            for (Integer msv : data.sample_list) {
                try {
                    writer.write(msv.toString());
                    writer.write("\r\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
