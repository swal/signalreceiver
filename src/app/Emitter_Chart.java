/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.prefs.Preferences;

public class Emitter_Chart extends DataListener {
    private final Preferences preferences;

    final EmitterChartDialog dialog;
    private final WindowPersistentGeometry windowgeometry;

    public Emitter_Chart(Preferences preferences) {
        this.preferences = preferences;
        dialog = new EmitterChartDialog();
        windowgeometry = new WindowPersistentGeometry(dialog, preferences, 0, 0, 500, 350);
    }

    @Override
    public void setActiveEx(boolean active) {
        super.setActiveEx(active);
        if (active) {
            dialog.setVisible(true);
        }
    }

    @Override
    public void processData(DataSet data) {
        if (! isActiveEx()) {
            return;
        }
        dialog.notifyData(data);
    }
}
