/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

/*
 *  https://github.com/java-native/jssc
 */

import jssc.SerialPort;
import jssc.SerialPortException;

import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import static jssc.SerialPort.*;


public class DataSourceIOLPCBA extends DataSource {
    private final Preferences preferences;
    private final String keySamplerate = "samplerate";
    private final String keySamples = "samples";
    private final String keyPortname ="portname";

    private int samplerate = 19200;
    private int samples = 8000;
    private String portname;

    private final DataSourceIOLPCBAForm dialog;
    private Receiver_IOL_PCBA_thread receiver_thread;
    private boolean running;

    private SerialPort port;

    public DataSourceIOLPCBA(Preferences preferences) {
        this.preferences = preferences;
        samplerate = (int) preferences.getLong(keySamplerate, samplerate);
        samples = (int) preferences.getLong(keySamples, samples);
        portname = preferences.get(keyPortname,"");

        dialog = new DataSourceIOLPCBAForm(this);
        running = false;
    }

    @Override
    public String toString() {
        return "IOL-PCBA (UART)";
    }

    @Override
    public boolean configure(boolean showDialog) {
        if (showDialog) {
            dialog.pack();
            dialog.setVisible(true);
        } else {
            return dialog.apply();
        }
        return true;
    }

    @Override
    public JPanel getConfigPanel() {
        return dialog.getRootPanel();
    }

    @Override
    public boolean start() {
        port = new SerialPort(portname);
        try {
            port.openPort();
            port.setParams(BAUDRATE_256000, DATABITS_8, STOPBITS_1, PARITY_NONE);
            /* force the command line interpreter within the device to the "idle" state */
            port.writeString("\n");
/*
            try {
                byte[] dummy_reads;
                do {
                    dummy_reads = port.readBytes(1, 100);
                } while (dummy_reads.length > 0);
            } catch (SerialPortTimeoutException e) {
                e.printStackTrace();
            }
 */
        } catch (SerialPortException exception) {
            exception.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed to connect to " + portname + "\n" + exception.getExceptionType());
            return false;
        }

        running = true;
        receiver_thread = new Receiver_IOL_PCBA_thread();
        receiver_thread.start();

        return true;
    }


    @Override
    public void stop() {
        running = false;

        while (receiver_thread.getState() != Thread.State.TERMINATED) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (port != null) {
            try {
                port.closePort();
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    public int getSamplerate() {
        return samplerate;
    }

    public void setSamplerate(int samplerate) {
        this.samplerate = samplerate;
        preferences.putInt(keySamplerate, samplerate);
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
        preferences.putInt(keySamples, samples);
    }

    public String getPortname() {
        return portname;
    }

    public void setPortname(String portname) {
        this.portname = portname;
        if (portname == null) {
            preferences.put(keyPortname, "");
        } else {
            preferences.put(keyPortname, portname);
        }
    }


    private class Receiver_IOL_PCBA_thread extends Thread {
        @Override
        public void run() {
            System.out.println("Enter thread");
            while (running) {
                int read_samples = samples;
                try {
                    if (samples == 0) {
                        port.writeString("d\n");
                    } else {
                        port.writeString("d " + samples + "\n");
                    }
                } catch (SerialPortException ex) {
                    ex.printStackTrace();
                }

                System.out.println("Enter receive loop");
                boolean receiving = true;
                boolean end_detected = false;
                byte[] buffer;
                StringBuilder out_string = new StringBuilder();
                long start_readbytes = System.currentTimeMillis();
                while (receiving) {
                    try {
                        buffer = port.readBytes();

                        if (buffer == null) {
                            if ((System.currentTimeMillis() - start_readbytes) > 1000) {
                                System.out.println("timeout waiting for the end of the data transmission, AKA \"the prompt\"");
                                end_detected = true;
                            }
                        } else {
                            start_readbytes = System.currentTimeMillis();

                            String s = new String(buffer, StandardCharsets.UTF_8);
                            out_string.append(s);

                            if (out_string.toString().endsWith("> ")) {
                                end_detected = true;
                            }
                        }

                        if (end_detected) {
                            String[] lines = out_string.toString().split("\n");
                            System.out.println("lines: " + lines.length);

                            List<Integer> sample_list = new ArrayList<>();
                            for (String line : lines) {
//                                System.out.println("#"+i+":" + line);
                                try {
                                    sample_list.add(Integer.parseInt(line));
                                } catch (NumberFormatException ex) {
                                    System.out.println("ignoring line \"" + line + "\"");
                                }
                            }
                            read_samples = sample_list.size();
                            System.out.println("samples: " + read_samples);

                            if (sample_list.size() > 0) {
                                DataSet dataset = new DataSet();
                                dataset.sample_rate = samplerate;
                                dataset.sample_list = sample_list;
                                dataset.minValue = null;
                                dataset.maxValue = null;
                                dataset.meanValue = null;
                                notifyData(dataset);
                            }
                            receiving = false;
                        }
                    } catch (SerialPortException ex) {
                        receiving = false;
                        ex.printStackTrace();
                    }
                }
                System.out.println("Leave receive loop");

                try {
                    Thread.sleep((1000 + 100) * Math.max(samples, read_samples) / samplerate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Leave thread");
        }
    }
}
