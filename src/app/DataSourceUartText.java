/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.prefs.Preferences;

import static jssc.SerialPort.*;


public class DataSourceUartText extends DataSource {
    private final Preferences preferences;
    private final String keySamplerate = "samplerate";
    private final String keySamples = "samples";
    private final String keyPortname ="portname";

    private int samplerate = 10;
    private int samples = 20000;
    private String portname;

    private final DataSourceUartTextForm dialog;
    private ReceiverThread receiver_thread;
    private ParserThread parserThread;

    private boolean running;

    private SerialPort port;

    public DataSourceUartText(Preferences preferences) {
        this.preferences = preferences;
        samplerate = (int) preferences.getLong(keySamplerate, samplerate);
        samples = (int) preferences.getLong(keySamples, samples);
        portname = preferences.get(keyPortname,"");

        dialog = new DataSourceUartTextForm(this);
        running = false;
    }

    @Override
    public String toString() {
        return "UART ASCII Streaming";
    }

    @Override
    public boolean configure(boolean showDialog) {
        if (showDialog) {
            dialog.pack();
            dialog.setVisible(true);
        } else {
            return dialog.apply();
        }
        return true;
    }

    @Override
    public JPanel getConfigPanel() {
        return dialog.getRootPanel();
    }

    @Override
    public boolean start() {
        parserThread = new ParserThread();
        parserThread.start();

        port = new SerialPort(portname);
        try {
            port.openPort();
            port.setParams(115200, DATABITS_8, STOPBITS_1, PARITY_NONE);
        } catch (SerialPortException exception) {
            exception.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed to connect to " + portname + "\n" + exception.getExceptionType());
            return false;
        }

        running = false;
        receiver_thread = new ReceiverThread();
        try {
            port.addEventListener(receiver_thread, MASK_RXCHAR);
            running = true;
        } catch (SerialPortException e) {
            e.printStackTrace();
        }

        return true;
    }


    @Override
    public void stop() {
        running = false;
        if (port != null) {
            try {
                port.removeEventListener();
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
            try {
                port.closePort();
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    public int getSamplerate() {
        return samplerate;
    }

    public void setSamplerate(int samplerate) {
        this.samplerate = samplerate;
        preferences.putInt(keySamplerate, samplerate);
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
        preferences.putInt(keySamples, samples);
    }

    public String getPortname() {
        return portname;
    }

    public void setPortname(String portname) {
        this.portname = portname;
        if (portname == null) {
            preferences.put(keyPortname, "");
        } else {
            preferences.put(keyPortname, portname);
        }
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String byteToHex(byte b) {
        byte[] hexChars = new byte[2];
        int v = b & 0xFF;
        hexChars[0] = (byte) HEX_ARRAY[v >>> 4];
        hexChars[1] = (byte) HEX_ARRAY[v & 0x0F];
        return new String(hexChars, StandardCharsets.UTF_8);
    }

    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = (byte) HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = (byte) HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }


    private class ParserThread extends Thread {

        boolean running;
        ArrayBlockingQueue<byte[]> in_buffer;

        ParserThread() {
            running = false;
            in_buffer = new ArrayBlockingQueue(200);
        }

        void addData(byte[] buffer) {
            try {
                in_buffer.add(buffer);
            }
            catch (IllegalStateException e) {
                System.out.println("DataSourceUartText queue overflow");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        synchronized public void run() {
            System.out.println("Enter ParserThread");

            String in_string = "";
            List<Integer> sample_list = new ArrayList<>();

            running = true;
            while (running) {
                byte[] buffer = null;
                try {
                    buffer = in_buffer.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if ((buffer != null) && (buffer.length > 0)) {
                    String s =new String(buffer, StandardCharsets.UTF_8);
//                    String s =new String(buffer);
//                    System.out.println("\nRX #: " + buffer.length + "  >" + s + "<");

                    in_string = in_string.concat(s);
//                    System.out.println("in_string = \"" + in_string + "\"");

                    String lines[] = in_string.split("\\r?\\n|\\r");
//                    System.out.println("lines = " + lines.length);

                    for (int i = 0; i < lines.length - 1; i++) {
//                        System.out.println("line_i = \"" + lines[i] + "\"");

                        String fields[] = lines[i].split("[ \\t]+");
                        for (int j=0; j< fields.length; j++) {
//                            System.out.println("fields[" + j + "] = \"" + fields[j] + "\"");
                        }

                        if ((fields.length > 2) && (fields[2] != null)) {
                            try {
                                int sample_value = Integer.parseInt(fields[2]);

                                System.out.println("value = " + sample_value);

                                sample_list.add(sample_value);

                                if (sample_list.size() >= samples) {
                                    System.out.println("next diagram");
                                    DataSet dataset = new DataSet();
                                    dataset.sample_rate = samplerate;
                                    dataset.sample_list = sample_list;
                                    dataset.minValue = null;
                                    dataset.maxValue = null;
                                    dataset.meanValue = null;
                                    notifyData(dataset);
                                    sample_list = new ArrayList<>();
                                }
                            }
                            catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (lines.length > 1) {
                        in_string = lines[lines.length - 1];
                    }
                } /* buffer != null */
            } /* while running */
        } /* run() */
    }

    private class ReceiverThread implements SerialPortEventListener {
        byte[] buffer;
        public void serialEvent(SerialPortEvent event) {
//            if (event.isRXCHAR()) {
            try {
//                    byte buffer[] = port.readBytes();
                buffer = port.readBytes();
                if ((buffer != null) && (buffer.length > 0)) {
                    parserThread.addData(buffer);
                }
            } catch (SerialPortException ex) {
                System.out.println(ex);
            }
//            }
        }
    }
}
