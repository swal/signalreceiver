/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import java.util.Objects;

public class ByteArrayConvert {

    public static int toInteger(byte[] bytes, int index, int length, ByteOrder byteorder, boolean signed) {
        int result = 0;
        if (length <= 0) {
            // error
        } else if (length == 1) {
            // byte order does not exist with one byte only
            if (signed) {
                result = bytes[index];
            } else {
                result = bytes[index] & 0xFF;
            }
        } else if (length == 2) {
            if (ByteOrder.LITTLE_ENDIAN.equals(byteorder)) {
                // LSB first
                if (signed) {
                    result = (bytes[index] & 0xFF) | (bytes[index + 1] << 8);
                } else {
                    result = (bytes[index] & 0xFF) | ((bytes[index + 1] & 0xFF) << 8);
                }
            } else if (ByteOrder.BIG_ENDIAN.equals(byteorder)) {
                // MSB first
                if (signed) {
                    result = (bytes[index] << 8) | (bytes[index + 1] & 0xFF);
                } else {
                    result = ((bytes[index] & 0xFF) << 8) | (bytes[index + 1] & 0xFF);
                }
            }
        } else if (length == 3) {
            if (ByteOrder.LITTLE_ENDIAN.equals(byteorder)) {
                // LSB first
                if (signed) {
                    result = (bytes[index] & 0xFF) | ((bytes[index + 1] & 0xFF) << 8) | (bytes[index + 2] << 16);
                } else {
                    result = (bytes[index] & 0xFF) | ((bytes[index + 1] & 0xFF) << 8) | ((bytes[index + 2] & 0xFF) << 16);
                }
            } else if (ByteOrder.BIG_ENDIAN.equals(byteorder)) {
                // MSB first
                if (signed) {
                    result = ((bytes[index]       ) << 16) | ((bytes[index + 1] & 0xFF) << 8) | (bytes[index + 2] & 0xFF);
                } else {
                    result = ((bytes[index] & 0xFF) << 16) | ((bytes[index + 1] & 0xFF) << 8) | (bytes[index + 2] & 0xFF);
                }
            }
        } else if (length == 4) {
            if (ByteOrder.LITTLE_ENDIAN.equals(byteorder)) {
                // LSB first
                if (signed) {
                    result = (bytes[index] & 0xFF) | ((bytes[index + 1] & 0xFF) << 8) | ((bytes[index + 2] & 0xFF) << 16) | (bytes[index + 3] << 24);
                } else {
                    result = (bytes[index] & 0xFF) | ((bytes[index + 1] & 0xFF) << 8) | ((bytes[index + 2] & 0xFF) << 16) | ((bytes[index + 3] & 0xFF) << 24);
                }
            } else if (ByteOrder.BIG_ENDIAN.equals(byteorder)) {
                // MSB first
                if (signed) {
                    result = ((bytes[index]       ) << 24) | ((bytes[index + 1] & 0xFF) << 16) | ((bytes[index + 2] & 0xFF) << 8) | (bytes[index + 3] & 0xFF);
                } else {
                    result = ((bytes[index] & 0xFF) << 24) | ((bytes[index + 1] & 0xFF) << 16) | ((bytes[index + 2] & 0xFF) << 8) | (bytes[index + 3] & 0xFF);
                }
            }
        } else {
            for (int i = 0; i < length; i++) {
                int b = bytes[i];
                if (ByteOrder.LITTLE_ENDIAN.equals(byteorder)) {
                    // LSB first
                    b = b << (8 * i);
                } else if (ByteOrder.BIG_ENDIAN.equals(byteorder)) {
                    // MSB first
                    result = result << 8;
                }
                result |= b;
            }
        }
        return result;
    }

    public static class ByteOrder {

        /**
         * LSB first
         */
        public static final ByteOrder LITTLE_ENDIAN = new ByteOrder("LITTLE_ENDIAN");

        /**
         * MSB first
         */
        public static final ByteOrder BIG_ENDIAN = new ByteOrder("BIG_ENDIAN");

        private final String name;

        public ByteOrder(final String name) {
            this.name = name;
        }

        @Override
        public final boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ByteArrayConvert.ByteOrder)) {
                return false;
            }
            return Objects.equals(name, ((ByteArrayConvert.ByteOrder) obj).name);
        }

        @Override
        public final String toString() {
            return name;
        }
    }
}
