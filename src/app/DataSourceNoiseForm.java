/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.swing.*;
import java.awt.event.*;

public class DataSourceNoiseForm extends JDialog {
    private JPanel DataSourceNoisePanel;

    private JButton buttonOK;
    private JButton buttonCancel;

    private JPanel parameterPanel;
    private JPanel panelSamplesPerTransfer;
    private JSpinner valueSamplesPerTransfer;
    private JPanel panelSamplesPerSecond;
    private JSpinner valueSamplesPerSecond;
    private JPanel panelAmplitude;
    private JSpinner valueAmplitude;
    private JPanel bottomPanel;
    private JPanel buttonPanel;

    private final DataSourceNoise dataSourceNoise;

    public DataSourceNoiseForm(DataSourceNoise dataSourceNoise) {
        setContentPane(DataSourceNoisePanel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        DataSourceNoisePanel.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        this.dataSourceNoise = dataSourceNoise;
        valueSamplesPerTransfer.setValue(dataSourceNoise.getSamples());
        valueSamplesPerSecond.setValue(dataSourceNoise.getSamplerate());
        valueAmplitude.setValue(dataSourceNoise.getAmplitude());
    }

    public boolean apply() {
        dataSourceNoise.setSamples((Integer)(valueSamplesPerTransfer.getValue()));
        dataSourceNoise.setSamplerate((Integer)(valueSamplesPerSecond.getValue()));
        dataSourceNoise.setAmplitude((Integer)(valueAmplitude.getValue()));
        return true;
    }

    private void onOK() {
        apply();
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public JPanel getRootPanel() {
        return parameterPanel;
    }
}
