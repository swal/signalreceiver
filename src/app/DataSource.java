/*
 * Copyright (c) 2022, 2023, 2024 Stefan Waldschmidt
 *
 * SPDX-License-Identifier: MITNFA
 */

package app;

import javax.swing.*;

public abstract class DataSource {
    private DataListener dataListener;
    private static final JPanel emptyPanel = new JPanel();

    public void setDataListener(DataListener dataListener) {
        this.dataListener = dataListener;
    }

    protected void notifyData(DataSet data) {
        dataListener.notifyData(data);
    }

    boolean configure(boolean showDialog) {
        return true;
    }

    JPanel getConfigPanel() {
        return emptyPanel;
    }

    boolean start() {
        return false;
    }

    void stop() {
    }
}
